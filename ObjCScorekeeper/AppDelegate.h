//
//  AppDelegate.h
//  ObjCScorekeeper
//
//  Created by Carissa on 9/22/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

