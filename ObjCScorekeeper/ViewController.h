//
//  ViewController.h
//  ObjCScorekeeper
//
//  Created by Carissa on 9/22/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *rightLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftLabel;
@property (strong, nonatomic) IBOutlet UITextField *team1;
@property (strong, nonatomic) IBOutlet UITextField *team2;


- (IBAction)rightStepper:(UIStepper *)sender;
- (IBAction)leftStepper:(UIStepper *)sender;



- (IBAction)resetButton:(id)sender;

@end

