//
//  ViewController.m
//  ObjCScorekeeper
//
//  Created by Carissa on 9/22/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

@synthesize team1;
@synthesize team2;

- (void)viewDidLoad {
    [super viewDidLoad];
    
        [team1 setDelegate:self];
        [team2 setDelegate:self];
   
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [team1 resignFirstResponder];
    [team2 resignFirstResponder];
    return true;
}


- (IBAction)leftStepper:(UIStepper *)sender {
    NSLog(@" %.0f",sender.value);
    self.leftLabel.text = [NSString stringWithFormat:@"%.0f",sender.value];
}


- (IBAction)rightStepper:(UIStepper *)sender2 {
    NSLog(@" %.0f",sender2.value);
    self.rightLabel.text = [NSString stringWithFormat:@"%.0f",sender2.value];
}

- (IBAction)resetButton:(id)sender {
    self.leftLabel.text = @"0";
    self.rightLabel.text = @"0";
    self.team1.text = @"";
    self.team2.text = @"";
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
